// 在第一次加载时自动执行规则
$(function(){
    route(window.location.hash);
})

// 点击导航出触发执行路由规则
$("#header .top-mune ul li a").click(function(){
  var icc = $(this).attr('href');
  route(icc);
});


// 路由规则
function route(e){
  var route = e.match(/[^#].*/);
  if(route == null){
    $("#aritcle").load("src/home/home.html");
    $("#footer").load("src/footer/footer.html");
  }else{
     $("#aritcle").load("src/"+route);
     $("#footer").load("src/footer/footer_none.html");
  }
}