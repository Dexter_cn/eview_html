$(function(){
   initMap();
})


function initMap() {
  var myLatLng = {lat:22.641585, lng:114.029524};

  var map = new google.maps.Map(document.getElementById('map'), {zoom: 15,center: myLatLng});

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Eview'
  });
}